// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Learn NodeJS UA',
  tagline: 'Вивчай NodeJS українською',
  url: 'https://blindkai.gitlab.io',
  baseUrl: '/learn-nodejs-ua/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'BlindKai',
  projectName: 'learn-nodejs-ua',

  i18n: {
    defaultLocale: 'uk-UA',
    locales: ['uk-UA'],
    localeConfigs: {
      'uk-UA': {
        label: 'Українська',
        direction: 'ltr',
        htmlLang: 'uk-UA'
      }
    }
  }, 

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl: 'https://gitlab.com/BlindKai/learn-nodejs-ua/-/tree/develop',
        },
        blog: {
          showReadingTime: true,
          editUrl: 'https://gitlab.com/BlindKai/learn-nodejs-ua/-/tree/develop',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'Learn NodeJS UA',
        // logo: {
        //   alt: 'Learn NodeJS UA Logo',
        //   src: 'img/logo.svg',
        // },
        items: [
          {
            type: 'doc',
            docId: 'intro',
            position: 'left',
            label: 'Посібник',
          },
          // { to: '/blog', label: 'Новини', position: 'left' },
          {
            href: 'https://gitlab.com/BlindKai/learn-nodejs-ua',
            label: 'GitLab',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Навчальні матеріали',
            items: [
              {
                label: 'Посібник',
                to: '/docs/intro',
              },
            ],
          },
          {
            title: 'Інше',
            items: [
              // {
              //   label: 'Новини',
              //   to: '/blog',
              // },
              {
                label: 'GitLab',
                href: 'https://gitlab.com/BlindKai/learn-nodejs-ua',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Learn NodeJS UA. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
