import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Детальність',
    description: (
      <>
        Часто API Reference на офіційному сайті не містить практичних прикладів використання коду, 
        або ж не пояснює його використання в реальних умовах.
        Learn NodeJS UA покликаний змінити це.
      </>
    ),
  },
  {
    title: 'Українська мова',
    description: (
      <>
        На жаль на даний час в інтернеті складно знайти навчальні матеріали українською, 
        що унеможливлює комфортне освоєння середовища людьми, що не володіють англійською 
        на достатньому для цього рівні.
      </>
    ),
  },
  {
    title: 'Спільнота',
    description: (
      <>
        Одним з завданням ресурсу є згуртувати навколо себе людей, яких об'єднює бажання вивчати Node, 
        ділитись досвідом та розвивати україномовні ресурси.
      </>
    ),
  },
];

function Feature({ Svg, title, description }) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
